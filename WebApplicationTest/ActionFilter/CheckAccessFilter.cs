﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApplicationTest.ActionFilter
{
    public class CheckAccessAttribute : TypeFilterAttribute
    {
        public readonly bool _isIgnore;

        public bool IsIgnore => _isIgnore;

        public CheckAccessAttribute(bool ignore = false) : base(typeof(CheckAccessFilter))
        {
            this._isIgnore = ignore;
            this.Arguments = new object[] { ignore };
        }

        public class CheckAccessFilter : IActionFilter
        {
            public CheckAccessFilter(bool ignore)
            {
                
            }

            public void OnActionExecuting(ActionExecutingContext context)
            {
                var actionFilter = context.ActionDescriptor.FilterDescriptors
                    .Where(filterDescriptor => filterDescriptor.Scope == FilterScope.Action)
                    .Select(filterDescriptor => filterDescriptor.Filter).OfType<CheckAccessAttribute>().FirstOrDefault();

                context.Result = new RedirectResult("/Home/Error");
            }

            public void OnActionExecuted(ActionExecutedContext context)
            {
                
            }
        }
    }
}
